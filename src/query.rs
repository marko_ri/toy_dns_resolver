pub const TYPE_A: u16 = 1;
pub const TYPE_NS: u16 = 2;
const CLASS_IN: u16 = 1;

#[derive(Default, Debug)]
pub struct DnsHeader {
    pub id: u16,
    pub flags: u16,
    pub num_questions: u16,
    pub num_answers: u16,
    pub num_authorities: u16,
    pub num_additionals: u16,
}

impl DnsHeader {
    fn to_bytes(&self) -> Vec<u8> {
        [
            self.id.to_be_bytes(),
            self.flags.to_be_bytes(),
            self.num_questions.to_be_bytes(),
            self.num_answers.to_be_bytes(),
            self.num_authorities.to_be_bytes(),
            self.num_additionals.to_be_bytes(),
        ]
        .concat()
    }
}

#[derive(Debug)]
pub struct DnsQuestion {
    pub name: String,
    pub type_: u16,
    pub class_: u16,
}

impl DnsQuestion {
    fn to_bytes(&self) -> Vec<u8> {
        let mut result = Vec::new();
        result.append(&mut self.name.clone().into_bytes());
        result.extend_from_slice(&self.type_.to_be_bytes());
        result.extend_from_slice(&self.class_.to_be_bytes());

        result
    }
}

fn encode_dns_name_str(domain_name: &str) -> String {
    String::from_utf8(encode_dns_name(domain_name.as_bytes())).expect("utf8")
}

fn encode_dns_name(domain_name: &[u8]) -> Vec<u8> {
    let mut result = Vec::new();
    for part in domain_name.split(|byte| byte == &b'.') {
        result.push(u8::try_from(part.len()).expect("each dns name part must fit in one byte"));
        result.extend_from_slice(part);
    }
    result.push(b'\x00');
    result
}

pub fn build_query(domain_name: &str, record_type: u16) -> Vec<u8> {
    let name = encode_dns_name_str(domain_name);
    // not very random :)
    let id = 1;
    let header = DnsHeader {
        id,
        num_questions: 1,
        flags: 0,
        ..Default::default()
    };
    let question = DnsQuestion {
        name,
        type_: record_type,
        class_: CLASS_IN,
    };

    let mut result = Vec::new();
    result.append(&mut header.to_bytes());
    result.append(&mut question.to_bytes());
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_build_query() {
        let domain_name = "example.com";
        let actual = build_query(domain_name, TYPE_A);
        assert_eq!(
            b"\x00\x01\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x07example\x03com\x00\x00\x01\x00\x01",
            actual.as_slice()
        );
    }

    #[test]
    fn test_encode_dns_name() {
        let domain_name: &[u8; 10] = b"google.com";
        let expected = b"\x06google\x03com\x00";
        let actual = encode_dns_name(domain_name);
        assert_eq!(expected, actual.as_slice());
    }

    #[test]
    fn test_pack() {
        // IN:  struct.pack('!HH', 5, 23)
        // OUT: b'\x00\x05\x00\x17'
        let mut actual = [0_u8; 4];
        actual[..2].copy_from_slice(&5_u16.to_be_bytes());
        actual[2..].copy_from_slice(&23_u16.to_be_bytes());
        let expected = b"\x00\x05\x00\x17";
        assert_eq!(expected, &actual);
    }

    #[test]
    fn test_pack_format() {
        // IN:  header_to_bytes(DNSHeader(id=0x1314, flags=0, num_questions=1, num_additionals=0, num_authorities=0, num_answers=0))
        // OUT: b'\x13\x14\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00'
        let header = DnsHeader {
            id: 0x1314,
            flags: 0,
            num_questions: 1,
            num_additionals: 0,
            num_authorities: 0,
            num_answers: 0,
        };
        let actual = header.to_bytes();
        assert_eq!(
            b"\x13\x14\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00",
            actual.as_slice()
        );
    }
}
