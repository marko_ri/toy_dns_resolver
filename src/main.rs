mod parse;
mod query;

use parse::DnsPacket;
use query::{build_query, TYPE_A};
use std::net::UdpSocket;

const ROOT: &str = "198.41.0.4";

fn main() {
    let domains = ["example.com", "google.com", "facebook.com", "twitter.com"];
    for domain in domains {
        let ip = resolve(domain, TYPE_A);
        println!("Domain: {domain}, has IP: {ip}");
    }
}

fn resolve(domain: &str, record_type: u16) -> String {
    let mut nameserver = ROOT.to_string();
    loop {
        println!("Quering {nameserver} for {domain}");
        let dns_packet = send_query(&nameserver, domain, record_type);
        let ip = dns_packet.answer();
        if !ip.is_empty() {
            return ip;
        }
        let ns_ip = dns_packet.nameserver_ip();
        if !ns_ip.is_empty() {
            nameserver = ns_ip;
            continue;
        }
        let ns_domain = dns_packet.nameserver();
        if !ns_domain.is_empty() {
            nameserver = resolve(&ns_domain, TYPE_A);
        } else {
            panic!("something went wrong");
        }
    }
}

fn send_query(ip_addr: &str, domain: &str, record_type: u16) -> DnsPacket {
    let query = build_query(domain, record_type);
    let socket = UdpSocket::bind("0.0.0.0:0").expect("couldn't bind to address");
    socket
        .send_to(&query, &format!("{}:53", ip_addr))
        .expect("couldn't send message");

    let mut buf = [0; 512];
    socket.recv(&mut buf).expect("couldn't receive message");

    DnsPacket::parse(buf.as_slice())
}
