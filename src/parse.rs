use super::query::{DnsHeader, DnsQuestion, TYPE_A, TYPE_NS};
use std::io::{Cursor, Read, Seek, SeekFrom};

#[derive(Debug)]
pub struct DnsRecord {
    pub name: String,
    pub type_: u16,
    pub class_: u16,
    pub ttl: u32,
    pub data: Vec<u8>,
}

pub fn parse_header(reader: &mut Cursor<&[u8]>) -> DnsHeader {
    const HEADER_SIZE: usize = std::mem::size_of::<DnsHeader>();
    let mut header = [0; HEADER_SIZE];
    reader.read_exact(&mut header).unwrap();

    DnsHeader {
        id: u16::from_be_bytes([header[0], header[1]]),
        flags: u16::from_be_bytes([header[2], header[3]]),
        num_questions: u16::from_be_bytes([header[4], header[5]]),
        num_answers: u16::from_be_bytes([header[6], header[7]]),
        num_authorities: u16::from_be_bytes([header[8], header[9]]),
        num_additionals: u16::from_be_bytes([header[10], header[11]]),
    }
}

#[allow(dead_code)]
fn decode_name_simple(reader: &mut Cursor<&[u8]>) -> String {
    let mut parts = vec![];
    let mut len = [0_u8; 1];
    while reader.read_exact(&mut len).is_ok() {
        if len[0] == 0 {
            break;
        }
        let mut part = vec![0; len[0] as usize];
        reader.read_exact(&mut part).unwrap();
        parts.push(String::from_utf8(part).unwrap());
    }
    parts.join(".")
}

pub fn decode_name(reader: &mut Cursor<&[u8]>) -> String {
    let mut parts = vec![];
    let mut len = [0_u8; 1];
    while reader.read_exact(&mut len).is_ok() {
        if len[0] == 0 {
            break;
        }
        if len[0] & 0b1100_0000 != 0 {
            parts.push(decode_compressed_name(len[0], reader));
            break;
        }
        let mut part = vec![0; len[0] as usize];
        reader.read_exact(&mut part).unwrap();
        parts.push(String::from_utf8(part).unwrap());
    }
    parts.join(".")
}

fn decode_compressed_name(len: u8, reader: &mut Cursor<&[u8]>) -> String {
    let mut next_byte = [0; 1];
    reader.read_exact(&mut next_byte).unwrap();

    let mut pointer_bytes = vec![(len & 0b0011_1111)];
    pointer_bytes.extend_from_slice(&next_byte);

    let pointer = ((pointer_bytes[0] as u16) << 8) | (pointer_bytes[1] as u16);

    let current_pos = reader.position();
    reader.seek(SeekFrom::Start(pointer as u64)).unwrap();
    let result = decode_name(reader);
    reader.seek(SeekFrom::Start(current_pos)).unwrap();

    result
}

fn parse_question(reader: &mut Cursor<&[u8]>) -> DnsQuestion {
    let name = decode_name(reader);
    let mut data = [0; 4];
    reader.read_exact(&mut data).unwrap();
    let type_ = u16::from_be_bytes(data[0..2].try_into().unwrap());
    let class_ = u16::from_be_bytes(data[2..4].try_into().unwrap());
    DnsQuestion {
        name,
        type_,
        class_,
    }
}

fn parse_record(reader: &mut Cursor<&[u8]>) -> DnsRecord {
    let name = decode_name(reader);
    // type, class, TTL, and data length together are 10 bytes (2 + 2 + 4 + 2 = 10)
    let mut data = [0; 10];
    reader.read_exact(&mut data).unwrap();
    let type_ = u16::from_be_bytes(data[0..2].try_into().unwrap());
    let class_ = u16::from_be_bytes(data[2..4].try_into().unwrap());
    let ttl = u32::from_be_bytes(data[4..8].try_into().unwrap());
    let data_len = u16::from_be_bytes(data[8..10].try_into().unwrap());

    let mut data = vec![0; data_len as usize];
    let dat = match type_ {
        TYPE_NS => decode_name(reader).into_bytes(),
        TYPE_A => {
            reader.read_exact(&mut data).unwrap();
            ip_to_string(data.as_slice()).into_bytes()
        }
        _ => {
            reader.read_exact(&mut data).unwrap();
            data
        }
    };

    DnsRecord {
        name,
        type_,
        class_,
        ttl,
        data: dat,
    }
}

#[derive(Debug)]
pub struct DnsPacket {
    pub header: DnsHeader,
    pub questions: Vec<DnsQuestion>,
    pub answers: Vec<DnsRecord>,
    pub authoroties: Vec<DnsRecord>,
    pub additionals: Vec<DnsRecord>,
}

impl DnsPacket {
    pub fn parse(data: &[u8]) -> Self {
        let mut reader = Cursor::new(data);
        let header = parse_header(&mut reader);
        let questions = (0..header.num_questions)
            .map(|_| parse_question(&mut reader))
            .collect();
        let answers = (0..header.num_answers)
            .map(|_| parse_record(&mut reader))
            .collect();
        let authoroties = (0..header.num_authorities)
            .map(|_| parse_record(&mut reader))
            .collect();
        let additionals = (0..header.num_additionals)
            .map(|_| parse_record(&mut reader))
            .collect();

        DnsPacket {
            header,
            questions,
            answers,
            authoroties,
            additionals,
        }
    }

    // return the first A record in the Answer section
    pub fn answer(&self) -> String {
        for x in self.answers.iter() {
            if x.type_ == TYPE_A {
                return String::from_utf8(x.data.clone()).unwrap();
            }
        }
        String::new()
    }

    // return the first A record in the Additional section
    pub fn nameserver_ip(&self) -> String {
        for x in self.additionals.iter() {
            if x.type_ == TYPE_A {
                return String::from_utf8(x.data.clone()).unwrap();
            }
        }
        String::new()
    }

    // return the first NS record in the Authority section
    pub fn nameserver(&self) -> String {
        for x in self.authoroties.iter() {
            if x.type_ == TYPE_NS {
                return String::from_utf8(x.data.clone()).unwrap();
            }
        }
        String::new()
    }
}

pub fn ip_to_string(ip: &[u8]) -> String {
    ip.iter()
        .map(|p| p.to_string())
        .collect::<Vec<_>>()
        .join(".")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_individual() {
        let data = b"`V\x81\x80\x00\x01\x00\x01\x00\x00\x00\x00\x03www\x07example\x03com\x00\x00\x01\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00R\x9b\x00\x04]\xb8\xd8\x22";
        let mut reader = Cursor::new(data.as_slice());
        let header = parse_header(&mut reader);
        // DNSHeader(id=24662, flags=33152, num_questions=1, num_answers=1, num_authorities=0, num_additionals=0)
        println!("Header: {header:?}");
        let question = parse_question(&mut reader);
        // DNSQuestion(name=b'www.example.com', type_=1, class_=1)
        println!("Question: {question:?}");
        let record = parse_record(&mut reader);
        // DNSRecord(name=b'www.example.com', type_=1, class_=1, ttl=21147, data=b']\xb8\xd8"')
        println!("Record: {record:?}");
    }

    #[test]
    fn test_parse_dns_packet() {
        let data = b"`V\x81\x80\x00\x01\x00\x01\x00\x00\x00\x00\x03www\x07example\x03com\x00\x00\x01\x00\x01\xc0\x0c\x00\x01\x00\x01\x00\x00R\x9b\x00\x04]\xb8\xd8\x22";
        let dns_packet = DnsPacket::parse(data.as_slice());
        // DNSPacket(header=DNSHeader(id=24662, flags=33152, num_questions=1, num_answers=1, num_authorities=0, num_additionals=0), questions=[DNSQuestion(name=b'www.example.com', type_=1, class_=1)], answers=[DNSRecord(name=b'www.example.com', type_=1, class_=1, ttl=21147, data=b']\xb8\xd8"')], authorities=[], additionals=[])
        println!("DnsPacket: {dns_packet:?}");
        println!(
            "IP: {}",
            ip_to_string(dns_packet.answers[0].data.as_slice())
        );
    }
}
